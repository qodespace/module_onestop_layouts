﻿using System.Collections.Generic;
using Onestop.Layouts.Models;
using Onestop.Layouts.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.ContentManagement.ViewModels;

namespace Onestop.Layouts.Settings {
    public class TemplatedItemPartSettings {
        public bool? AllowTemplateChoice { get; set; }
        public int DefaultTemplateId { get; set; }
        public IEnumerable<LayoutTemplatePart> Templates { get; set; } 
    }

    public class TemplatedItemPartSettingsHooks : ContentDefinitionEditorEventsBase
    {
        private readonly ITemplateService _templateService;

        public TemplatedItemPartSettingsHooks(ITemplateService templateService)
        {
            _templateService = templateService;
        }

        public override IEnumerable<TemplateViewModel> TypePartEditor(
            ContentTypePartDefinition definition) {

            if (definition.PartDefinition.Name != "TemplatedItemPart")
                yield break;

            var model = definition.Settings.GetModel<TemplatedItemPartSettings>();
            model.Templates = _templateService.GetTemplates();

            if (model.AllowTemplateChoice == null)
            {
                var partModel = definition.PartDefinition.Settings.GetModel<TemplatedItemPartSettings>();
                model.AllowTemplateChoice = partModel.AllowTemplateChoice;
            }

            yield return DefinitionTemplate(model);
        }

        public override IEnumerable<TemplateViewModel> TypePartEditorUpdate(
            ContentTypePartDefinitionBuilder builder,
            IUpdateModel updateModel) {

            if (builder.Name != "TemplatedItemPart")
                yield break;

            var model = new TemplatedItemPartSettings();
            model.Templates = _templateService.GetTemplates();
            updateModel.TryUpdateModel(model, "TemplatedItemPartSettings", null, null);
            builder
                .WithSetting("TemplatedItemPartSettings.AllowTemplateChoice", model.AllowTemplateChoice.ToString())
                .WithSetting("TemplatedItemPartSettings.DefaultTemplateId", model.DefaultTemplateId.ToString());
            yield return DefinitionTemplate(model);
        }
    }
}